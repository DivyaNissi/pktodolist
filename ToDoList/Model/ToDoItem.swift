//
//  ToDoItem.swift
//  ToDoList
//
//  Created by divya kothagattu on 22/09/20.
//  Copyright © 2020 divya kothagattu. All rights reserved.
//

import Foundation
import CoreData

enum Priority: Int {
    case low = 0
    case normal = 1
    case high = 2
}

struct ToDoItem: Identifiable {
    var id = UUID()
    var name: String = ""
    var priorityNum: Priority = .normal
    var isComplete: Bool = false
}



